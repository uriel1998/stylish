stylish
=======

Stylish Scripts for making the web better.  

You may need to install from the Userstyles.org website.  These below methods may work for you.

To install from [Stylish](https://userstyles.org/help/stylish):

* Visit a CSS file, click the Stylish icon, click Add File to Stylish (no metadata will be loaded, but updates will work)
* From the Stylish tab in the Add-ons Manager, click Install from URL, and provide the URL to a CSS file (again, no metadata, but updates work)

# Darker Remember the Milk (new UI) 

Apparently nobody's bothered to make a good dark style that works well with the new UI (and *much* more complicated CSS) of Remember The Milk. So I did, ensuring that I preserved the font-rendered icons and colors where important (labels and the like). Probably some stray popup stuff left over.

[This is my version on Userstyles.org](https://userstyles.org/styles/129763/darker-remember-the-milk-new-ui)

## Screenshots

###Before
![Before](https://raw.githubusercontent.com/uriel1998/stylish/master/rtm_before.png)
###After
![After](https://raw.githubusercontent.com/uriel1998/stylish/master/rtm_after.png)


# Google Contacts - Scroll Detail Card

I couldn't scroll the details card on the new Google Contacts layout, which sucks.  So I fixed it.

[This is my version on Userstyles.org](https://userstyles.org/styles/111468/scroll-details-card-on-google-contacts)

## Screenshots

###Before
![Before](https://raw.githubusercontent.com/uriel1998/stylish/master/google_contacts_compact_pre.png)
###After
![After](https://raw.githubusercontent.com/uriel1998/stylish/master/google_contacts_compact_post.png)


# Google Contacts - Compact Display

##NOTE:  Does not work with the new Material Design Contacts layout.

Originally forked from [this style](https://userstyles.org/styles/95813/google-contacts-compact-display)

[This is my version on Userstyles.org](https://userstyles.org/styles/106390/google-contacts-compact-display-updated-fork)

Changes the "Google Contacts" UI to a compact style. When opening the contacts inside the Gmail tab, the compact layout can be selected through they UI. However, when opening Google Contacts as a standalone page, there is no option to select the compact UI. 

## Screenshots

###Before
![Before](https://raw.githubusercontent.com/uriel1998/stylish/master/before_compact_google_contacts.jpg)
###After
![After](https://raw.githubusercontent.com/uriel1998/stylish/master/after_compact_google_contacts.jpg)

# Google Plus 2.0 wide (FORK)

[This is my version on Userstyles.org](https://userstyles.org/styles/69562/google-plus-2-0-wide-fork?r=1342591492)

This is a fork of Google Plus 2.0 Wide. It widens the two-column view, tested down to 1024 width.  

This is working as of 18 October 2014. The chat bar is left in, but you can uncomment that line in the style if you like. Mostly done because the original hadn't been updated in two months, and I needed something like this. Also added explicit support for https. 

## Screenshots
###Before
![Before](https://raw.githubusercontent.com/uriel1998/stylish/master/wider_gplus_before.jpg)
###After
![After](https://raw.githubusercontent.com/uriel1998/stylish/master/wider_gplus_after.jpg)

# RTM++ (FORK)

**Please note that this style does not work with the new Remember The Milk UI.**

Originally forked from [RTM++ by Kylir Horton](https://userstyles.org/styles/31340/rtm) with elements of the [blacken fonts by WCityMike](https://userstyles.org/styles/6817/remember-the-milk-blacken-and-enlarge)

[This is my version on Userstyles.org](https://userstyles.org/styles/106396/rtm-fork)

This is designed to be used with "A Bit Better RTM", and widens up the display and darkens it, using more screen real estate.  May be wonky with narrower monitors.
